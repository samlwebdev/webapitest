﻿

var url = "http://localhost:53202/api/recursos/";
var app = angular.module("app", []);

var mainController = function ($scope, $http) {
    $scope.recursos = {};
    var OnSucess = function (response) { $scope.recursos = response.data; };
    var OnFailure = function (response) { console.log(response); };

    $scope.addRecurso = function () {
        $http.post(url, { 'Descricao': $scope.descricao, 'Quantidade': $scope.quantidade, 'Observacao': $scope.observacao })
            .success(
            (data, status, headers, config) => {
                console.log(status);
            });
    };

    var getAllRecursos = function () {

        $http.get(url).then(OnSucess, OnFailure)
           
    };

    $scope.registrosLog = {};
    
    var getRegistros = function () {
        url = "http://localhost:53202/api/log/";
        $http.get(url)
            .then(
            (res) => {
                $scope.registrosLog = res.data;
            },
            (res) => {
                console.log(res);
            })
            
    }
    
    getAllRecursos();
    getRegistros();
};

app.controller("MainController", mainController);