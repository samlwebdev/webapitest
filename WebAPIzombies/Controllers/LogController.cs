﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using WebAPIzombies.Models;
using WebAPIzombies.Services;

namespace WebAPIzombies.Controllers
{
    public class LogController : ApiController
    {
        public IEnumerable<LogRecursos> Get()
        {
            var log = new AuditoriaService();
            return log.registros();
        }
    }
}
