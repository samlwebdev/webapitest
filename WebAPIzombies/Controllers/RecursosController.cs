﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using WebAPIzombies.Models;
using WebAPIzombies.Services;

namespace WebAPI_app.Controllers
{
    public class RecursosController : ApiController
    {
        RecursosManagerContainer db = new RecursosManagerContainer();
        AuditoriaService log = new AuditoriaService();
        
        // GET api/values
        public IEnumerable<Recursos> GetAll()
        {
            return db.Recursos.ToList();
        }

        // GET api/values/5
        public Recursos Get(string nome)
        {
            var recurso = db.Recursos.FirstOrDefault(r => r.Descricao == nome);
            if (recurso == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return recurso;

        }

        // POST api/values
        public HttpResponseMessage Post(Recursos r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Recursos.Add(r);
                    db.SaveChanges();
                    // auditoria
                    log.afterTransaction(r, "Entrada");
                    var response = Request.CreateResponse<Recursos>(HttpStatusCode.Created, r);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = r.Descricao }));
                    return response;
                }
                else {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // PUT api/values/5
        public HttpResponseMessage Put(string descricao, Recursos r)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (descricao != r.Descricao)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(r).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                //auditoria
                log.afterTransaction(r, "Entrada");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);

        }

        // DELETE api/values/5
        public HttpResponseMessage Delete(int id)
        {
            var recurso = db.Recursos.Find(id);
            if (recurso == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Recursos.Remove(recurso);

            try
            {
                db.SaveChanges();
                //auditoria
                log.afterTransaction(recurso, "Saida");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, recurso);

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
