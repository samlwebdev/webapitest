﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPIzombies.Models;

namespace WebAPIzombies.Services
{
    public class AuditoriaService
    {
        RecursosManagerContainer db = new RecursosManagerContainer();

        public void afterTransaction(Recursos r, string tipoTransacao)
        {
            var log = new LogRecursos();
            log.Data = DateTime.Now;
            log.Descricao = tipoTransacao+" : "+ r.Descricao;
            db.LogRecursos.Add(log);
            db.SaveChanges();
        }

        //retorna transações de entrada e saida
        public List<LogRecursos> registros()
        {
            var log = db.LogRecursos.ToList();
            return log;
        }
        

    }
}