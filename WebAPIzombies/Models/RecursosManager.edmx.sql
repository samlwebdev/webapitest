
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/29/2017 22:10:24
-- Generated from EDMX file: c:\users\samuelp\documents\visual studio 2015\Projects\WebAPIzombies\WebAPIzombies\Models\RecursosManager.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [master];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Recursos'
CREATE TABLE [dbo].[Recursos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Descricao] nvarchar(max)  NOT NULL,
    [Quantidade] int  NOT NULL,
    [Observacao] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LogRecursos'
CREATE TABLE [dbo].[LogRecursos] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Data] datetime  NOT NULL,
    [Descricao] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Recursos'
ALTER TABLE [dbo].[Recursos]
ADD CONSTRAINT [PK_Recursos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LogRecursos'
ALTER TABLE [dbo].[LogRecursos]
ADD CONSTRAINT [PK_LogRecursos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------